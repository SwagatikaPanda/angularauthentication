import { Injectable, NgZone } from '@angular/core';
import {
  AngularFirestore,
  AngularFirestoreDocument,
} from '@angular/fire/compat/firestore';
import * as auth from 'firebase/auth';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { Router } from '@angular/router';
import { User } from './user';
@Injectable({
  providedIn: 'root',
})
export class AuthService {
  userData: any;
  constructor(
    public afs: AngularFirestore, 
    public afAuth: AngularFireAuth, 
    public router: Router,
    public ngZone: NgZone 
  ) {
    this.afAuth.authState.subscribe((user) => {
      if (user) {
        this.userData = user;

        const email =this.userData.email;
        let displayName = email.split('@')[0];
        displayName = displayName.replace(/\d/g, '').replace(/\./g, '');

        // this.userData.providerData[0].displayName = displayName;
        // this.userData.displayName=displayName 
        // console.log(this.userData.displayName)

        const updatedUserData = { ...this.userData, displayName: displayName };

        console.log(updatedUserData.displayName); 

        console.log(this.userData)
        localStorage.setItem('user', JSON.stringify(this.userData));
        JSON.parse(localStorage.getItem('user')!);
      } else {
        localStorage.setItem('user', 'null');
        JSON.parse(localStorage.getItem('user')!);
      }
    });
  }

  SignIn(email: string, password: string) {
    return this.afAuth
      .signInWithEmailAndPassword(email, password)
      .then((result) => {

        const user = result.user;
        const email = user!.email;
        let displayName = email!.split('@')[0];
        displayName = displayName.replace(/\d/g, '').replace(/\./g, '');
        
        return user!.updateProfile({
          displayName: displayName
        }).then(() => {
          this.SetUserData(user);
          this.router.navigate(['/Login/dashboard2']);
        });
      })
      .catch((error) => {
        console.error('Sign-in error:', error);
        window.alert(error.message);
      });
}



  // Sign up with email/password
  SignUp(email: string, password: string) {
    return this.afAuth
      .createUserWithEmailAndPassword(email, password)
      .then((result) => {
        // Set the displayName
        return result.user!.updateProfile({
          displayName: 'DefaultName' // Set a default name or customize it as needed
        }).then(() => {
          this.SendVerificationMail();
          this.SetUserData(result.user);
        });
      })
      .catch((error) => {
        window.alert(error.message);
      });
}


  // Send email verfificaiton when new user sign up
  SendVerificationMail() {
    return this.afAuth.currentUser
      .then((u: any) => u.sendEmailVerification())
      .then(() => {
        this.router.navigate(['/Login/verify-email-address2']);
      });
  }

  // Reset Forggot password
  ForgotPassword(passwordResetEmail: string) {
    return this.afAuth
      .sendPasswordResetEmail(passwordResetEmail)
      .then(() => {
        window.alert('Password reset email sent, check your inbox.');
      })
      .catch((error) => {
        window.alert(error);
      });
  }

  // Returns true when user is looged in and email is verified
  // get isLoggedIn(): boolean {
  //   const user = JSON.parse(localStorage.getItem('user')!);
  //   return user !== null && user.emailVerified !== false ? true : false;
  // }
  get isLoggedIn(): boolean {
    const user = JSON.parse(localStorage.getItem('user')!);
    return user !== null;
}


  GoogleAuth() {
    return this.AuthLogin(new auth.GoogleAuthProvider()).then((res: any) => {
      this.router.navigate(['/Login/dashboard2']);
    });
  }
  // Auth logic to run auth providers
  AuthLogin(provider: any) {
    return this.afAuth
      .signInWithPopup(provider)
      .then((result) => {
        this.router.navigate(['/Login/dashboard2']);
        this.SetUserData(result.user);
      })
      .catch((error) => {
        window.alert(error);
      });
  }
  SetUserData(user: any) {
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(
      `users/${user.uid}`
    );
    const userData: User = {
      uid: user.uid,
      email: user.email,
      displayName: user.displayName,
      emailVerified: user.emailVerified,
    };
    return userRef.set(userData, {
      merge: true,
    });
  }

  // Sign out
  SignOut() {
    return this.afAuth.signOut().then(() => {
      localStorage.removeItem('user');
      this.router.navigate(['/Login/sign-in2']);
    });
  }
}
