import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/Shared/auth.service';
import { assetUrl } from 'src/single-spa/asset-url';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(public authService: AuthService) {}
  brandLogo = assetUrl('../../../assets/authenticity.png');
  userImg=assetUrl('../../../assets/user.png')
  ngOnInit(): void {

    
  }
  

}
