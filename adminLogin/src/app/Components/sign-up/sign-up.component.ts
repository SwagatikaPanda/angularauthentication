import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/Shared/auth.service';
@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
signupForm: any;
  constructor(
    public authService: AuthService, public router: Router
  ) { }
  ngOnInit() { }
  goToSignIn() {
    
    this.router.navigate(['/sign-in']);
  }
}